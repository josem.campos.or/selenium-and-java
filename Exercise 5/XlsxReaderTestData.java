package seleniumExercise5;

import static org.testng.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.junit.runners.Parameterized;


@RunWith(Parameterized.class)
public class XlsxReaderTestData {
	WebDriver driver;
	pageObjectExc form;
	
	
	    private final String fName;
	    private final String lName;
	    private final String jTitle;
	    private final String date;
	   
	    public XlsxReaderTestData(String fName, 
	    		String lName, 
	    		String jTitle, String date) {
	        
	        this.fName = fName;
	        this.lName = lName;
	        this.jTitle = jTitle;
	        this.date = date;
	    }
	
	
	@Parameters
	public static  Collection<Object[]> testFillTheFormFeed(){
		
		XlsxReader reader = new XlsxReader("./src/seleniumExercise5/dataSamples.xlsx");
		int rows = reader.getRowCount("data");
		
		Object[][] testDataFeed = new  Object[rows][4];
		for(int i = 0 ;i < rows ; i++){
			testDataFeed [i][0] = reader.getCellData("data", "firstName", i+2);
			testDataFeed [i][1] = reader.getCellData("data", "lastName", i +2);
			testDataFeed [i][2] = reader.getCellData("data", "jobTitle", i+2);
			testDataFeed [i][3] = reader.getCellData("data", "date", i+2);
		}
		return Arrays.asList(testDataFeed);
	}
	
	
	@Before
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\chromedriver.exe");
		driver = new ChromeDriver();
		form = new pageObjectExc(driver);
		driver.get("https://formy-project.herokuapp.com/form");
		
	}
	
	@Test
	//@UseDataProvider("testFillTheFormFeed")
	public void testFillTheForm(){
		//instatiate form POM
		pageObjectExc form = new pageObjectExc(driver);
		//Write Selenium automation code using JAVA/SELENIUM to fill the form of using Page Object Model Class 
		form.fillForm(fName,lName,jTitle,date);
		//Add Explicit wait method to wait for the submission confirmation message:
		//“The form was successfully submitted”.
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		//Assert or Validate the value: “Thanks for submitting your form”
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
		System.out.println("Test finished");
	}
	
	@After
	public void After() {
		driver.quit();
	}
	
	
	
}
