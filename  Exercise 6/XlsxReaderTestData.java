package seleniumExercise5;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.swing.text.html.CSS;

import org.junit.Before;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByCssSelector;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.junit.runners.Parameterized;


@RunWith(Parameterized.class)
public class XlsxReaderTestData {
	WebDriver driver;
	pageObjectExc form;
	WebDriverWait wait;
	
    private final String fName;
    private final String lName;
    private final String jTitle;
    private final String date;
	   
	    public XlsxReaderTestData(String fName, 
	    		String lName, 
	    		String jTitle, String date) {
	        
	        this.fName = fName;
	        this.lName = lName;
	        this.jTitle = jTitle;
	        this.date = date;
	    }
	
	
	@Parameters
	public static  Collection<Object[]> testFillTheFormFeed(){
		
		XlsxReader reader = new XlsxReader("./src/seleniumExercise5/dataSamples.xlsx");
		int rows = reader.getRowCount("data");
		
		Object[][] testDataFeed = new  Object[3][4];
		for(int i = 0 ;i < 3  ; i++){
			
			testDataFeed [i][0] = reader.getCellData("data", "firstName", i+2);
			testDataFeed [i][1] = reader.getCellData("data", "lastName", i +2);
			testDataFeed [i][2] = reader.getCellData("data", "jobTitle", i+2);
			testDataFeed [i][3] = reader.getCellData("data", "date", i+2);
			
		}
		return Arrays.asList(testDataFeed);
	}
	
	
	@Before
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\chromedriver.exe");
		driver = new ChromeDriver();
		form = new pageObjectExc(driver,fName,lName,jTitle,date);
		driver.get("https://formy-project.herokuapp.com/form");
	}
	
	@Test
	//@UseDataProvider("testFillTheFormFeed")
	public void testFillTheForm() throws IOException, InterruptedException{
		//instatiate form POM
		//Write Selenium automation code using JAVA/SELENIUM to fill the form of using Page Object Model Class 
		form.fillForm();
		File scrFileSex = ((TakesScreenshot)driver.findElement(By.xpath("//body/div/form/div/div[5]"))).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFileSex, new File("./src/seleniumExercise5/"+fName+"screenshotSex.png"));
		
		File scrFileEdu = ((TakesScreenshot)driver.findElement(By.xpath("//body/div/form/div/div[4]"))).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFileEdu, new File("./src/seleniumExercise5/"+fName+"screenshotEdu.png"));
		File scrFileDate = ((TakesScreenshot)driver.findElement(By.xpath("//body//div//div[7]"))).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFileDate, new File("./src/seleniumExercise5/"+fName+"screenshotDate.png"));
		form.slipi();
		WebElement selecSubmit= form.fillSubmit();
		selecSubmit.click();
		form.slipi();
		
		//Add Explicit wait method to wait for the submission confirmation message:
		//“The form was successfully submitted”.
		
		WebDriverWait wait = new WebDriverWait(driver,10);
		

		//Assert or Validate the value: “Thanks for submitting your form”
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
		System.out.println("Test finished");
	}
	
	@After
	public void After() {
		driver.quit();
	}
	
	
	
}
