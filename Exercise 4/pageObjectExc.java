package Selenium.Automation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class pageObjectExc{
	
	WebDriver driver;
	
	public pageObjectExc(WebDriver driver) {
		super();
		this.driver = driver;
	}
	 
	/*
	public pageObject(WebDriver driver) {
		this.driver = driver;
	}
	*/
	private By firstName = By.id("first-name");
	private By lastName = By.id("last-name");
	private By jobTitle = By.id("job-title");
	private By education = By.cssSelector("input[value='radio-button-1']");
	private By sex = By.xpath("//input[@value='checkbox-1']");
	private By yearsExp =  By.cssSelector("body div form div div select");
	private By yearsAns = By.xpath("//option[@value='1']");
	private By currDate = By.xpath("//input[@data-provide='datepicker']");
	private By submit = By.cssSelector("a[role='button']");

	public WebElement fillFirstName() {
		return driver.findElement(firstName);
	}
	
	public WebElement fillLastName() {
		return driver.findElement(lastName);
	}
	
	public WebElement fillJobTitle() {
		return driver.findElement(jobTitle);
	}
	
	public WebElement fillEducation() {
		return driver.findElement(education);
	}
	
	public WebElement fillSex() {
		return driver.findElement(sex);
	}
	
	public WebElement fillYearsExp() {
		return driver.findElement(yearsExp);
	}
	
	public WebElement fillYearsAns() {
		return driver.findElement(yearsAns);
	}
	
	public WebElement fillCurrDate() {
		return driver.findElement(currDate);
	}
	
	public WebElement fillSubmit() {
		return driver.findElement(submit);
	}
	
	  
	public void fillForm() {
							
		//Write Selenium automation code using JAVA/SELENIUM to fill the form of using Page Object Model Class 
		driver.get("https://formy-project.herokuapp.com/form");

		//fill the first name 
		WebElement firstName = fillFirstName();
		firstName.sendKeys("José Manuel");

		//fill the last name 
		WebElement lastName = fillLastName();
		lastName.sendKeys("Campos Ortega");

		//fill the jobTitle
		WebElement jobTitle = fillJobTitle();
		jobTitle.sendKeys("QA Engineer");
		
		//fill the education option
		WebElement selecEducation= fillEducation();
		selecEducation.click();
		
		//fill the sex option
		WebElement selecSex= fillEducation();
		selecSex.click();
		
		//fill the year option
		WebElement selecYear= fillYearsExp();
		selecYear.click();
		
		//fill the year answ option
		WebElement selecYearAns= fillYearsAns();
		selecYearAns.click();
		
		//fill the current dat option
		WebElement selecDate= fillCurrDate();
		selecDate.sendKeys("01/07/2022" + Keys.ENTER);
		
		//click submit
		WebElement selecSubmit= fillSubmit();
		selecSubmit.click();
		
	}
}
