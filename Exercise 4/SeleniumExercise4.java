package Selenium.Automation;


import static org.testng.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumExercise4 {
	private WebDriver driver;
	private pageObjectExc form;
	
	
	@Test
	public void testfillTheForm(){
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		pageObjectExc form = new pageObjectExc(driver);
		//Write Selenium automation code using JAVA/SELENIUM to fill the form of using Page Object Model Class 
		form.fillForm();
		//Add Explicit wait method to wait for the submission confirmation message:
		//“The form was successfully submitted”.
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		//Assert or Validate the value: “Thanks for submitting your form”
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
		System.out.println("Test finished");
		
		
	}
}
