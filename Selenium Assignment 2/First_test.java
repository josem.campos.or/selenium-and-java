package Selenium.Automation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class First_test {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
		driver.findElement(By.id("first-name")).sendKeys("José Manuel");
		driver.findElement(By.id("last-name")).sendKeys("Campos Ortega");
		driver.findElement(By.id("job-title")).sendKeys("QA Engineer");
		driver.findElement(By.cssSelector("input[value='radio-button-1']")).click();
		driver.findElement(By.xpath("//input[@value='checkbox-1']")).click();
		driver.findElement(By.cssSelector("body div form div div select")).click();
		driver.findElement(By.xpath("//option[@value='1']")).click();
		driver.findElement(By.xpath("//input[@data-provide='datepicker']"))
		.sendKeys("01/07/2022" + Keys.ENTER);
		driver.findElement(By.cssSelector("a[role='button']")).click();
		driver.close();
	}
	
}
